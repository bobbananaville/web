import random

def getDefense(val1, val2, bonus):
    return 10+val1+val2+bonus
def getDerivedStats(character):
    HP = (5+int(character.Will)+int(character.Presence)+int(character.Fortitude))*2+int(character.HPBonus)
    Guard = getDefense(int(character.Agility), int(character.Might), int(character.GuardBonus))
    Toughness = getDefense(int(character.Will), int(character.Fortitude), int(character.ToughnessBonus))
    Resolve = getDefense(int(character.Will), int(character.Presence), int(character.ResolveBonus))
    Level = int(int(character.XP)/3)+1
    StatPoints=40+3*int(character.XP)
    Speed=30+int(character.SpeedBonus)
    return {"HP":HP, "GUARD":Guard, "TOUGHNESS": Toughness, "RESOLVE":Resolve, "LEVEL":Level, "STATPOINTS":StatPoints, "SPEED":Speed}
def getAttributes(character):
    return [
        makeAttributeArray("Agility", character.Agility), makeAttributeArray("Fortitude", character.Fortitude), makeAttributeArray("Might", character.Might),
        makeAttributeArray("Deception", character.Deception), makeAttributeArray("Persuasion", character.Persuasion), makeAttributeArray("Presence", character.Presence),
        makeAttributeArray("Learning", character.Learning), makeAttributeArray("Logic", character.Logic), makeAttributeArray("Perception", character.Perception),
        makeAttributeArray("Will", character.Will), makeAttributeArray("Alteration", character.Alteration), makeAttributeArray("Creation", character.Creation),
        makeAttributeArray("Energy", character.Energy), makeAttributeArray("Entropy", character.Entropy), makeAttributeArray("Influence", character.Influence),
        makeAttributeArray("Movement", character.Movement), makeAttributeArray("Prescience", character.Prescience), makeAttributeArray("Protection", character.Protection)
    ]
def makeAttributeArray(attributeName, attributeValue='0'):
    diceDict={'0':'1d20!!','1':'1d20!!+1d4!!','2':'1d20!!+1d6!!','3':'1d20!!+1d8!!','4':'1d20!!+1d10!!','5':'1d20!!+2d6!!','6':'1d20!!+2d8!!','7':'1d20!!+2d10!!','8':'1d20!!+3d8!!','9':'1d20!!+3d10!!'}
    return [attributeName, attributeValue, diceDict[attributeValue], statCost(int(attributeValue))]
def statCost(current, total=0):
    if(current==0):
        return total
    else:
        return statCost(current-1, total+current)
def getStatTotal(attributeArray):
    total=0
    for item in attributeArray:
        total+=item[3]
    return total

def getDice(statValue):
    diceDict={0: [0, 4], 1:[1, 4], 2:[1, 6],3:[1, 8],4:[1, 10],5:[2, 6],6:[2, 8],7:[2, 10],8:[3, 8],9:[3, 10]}
    d20=0
    tempValue=0
    resultValues=[]
    while(d20%20==0):
        tempValue=random.randint(1, 20)
        d20+=tempValue
        resultValues.append(tempValue)
    secondDice=0
    #if(statValue in diceDict):
    i=diceDict[statValue][0]
    tempValue=0
    while(i>0):
        tempValue=random.randint(1, diceDict[statValue][1])
        resultValues.append(tempValue)
        if(tempValue!=diceDict[statValue][1]):
            i-=1
        secondDice+=tempValue
    # else:
    #     secondDice=1000
    return [resultValues, d20+secondDice]