from django.db import models
from . import myFunctions

class character(models.Model):
    ATTRIBUTE_VALUES = (
        ('0', '0'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
    )
    
    name=models.CharField(max_length=50, default="INSERT NAME HERE")
    Agility=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Fortitude=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Might=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Deception=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Persuasion=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Presence=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Learning=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Logic=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Perception=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Will=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")

    Alteration=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Creation=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Energy=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Entropy=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Influence=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Movement=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Prescience=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")
    Protection=models.CharField(max_length=1, choices=ATTRIBUTE_VALUES, default="0")

    HPBonus=models.SmallIntegerField( default="0")
    GuardBonus=models.SmallIntegerField( default="0")
    ToughnessBonus=models.SmallIntegerField( default="0")
    ResolveBonus=models.SmallIntegerField( default="0")
    SpeedBonus=models.SmallIntegerField( default="0")
    XP=models.SmallIntegerField( default="0")
    def __str__(self):
        return self.name
