from django.shortcuts import render
from django.http import HttpResponse

from .models import character
from . import myFunctions

# Create your views here.

def index(request):

    characters=character.objects.all()[:10]
    context={
        'title': 'Character List',
        'characters': characters
        }
    #return HttpResponse('HELLO FROM CHARACTER')
    return render(request, 'character/index.html', context)

def details(request, id):
    characterX = character.objects.get(id=id)
    derivedStats=myFunctions.getDerivedStats(characterX)
    attributeArray=myFunctions.getAttributes(characterX)
    statTotal=myFunctions.getStatTotal(attributeArray)
    remaining=derivedStats["STATPOINTS"]-statTotal
    context={
        'character': characterX,
        'derivedStats':derivedStats,
        'attributeArray':attributeArray,
        'statTotal':statTotal,
        'remaining':remaining,
    }
    return render(request, 'character/details.html', context)

def diceresult(request, statvalue):
    returnvalue=myFunctions.getDice(statvalue)
    context={
        'returnvalue':returnvalue[1],
        'resultlist':returnvalue[0],
    }
    return render(request, 'character/diceresult.html', context)